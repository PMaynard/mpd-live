package main

import (
	"flag"
	"html/template"
	"log"
	"net/http"
	"os/exec"

	"github.com/gorilla/websocket"
)

var addr = flag.String("addr", "localhost:8080", "HTTP Service Address")
var websocketEndPoint = flag.String("wsep", "localhost:8080", "WebSocket Endpoint Address")

var upgrader = websocket.Upgrader{}
var current chan []byte
var status_str []byte

func status(w http.ResponseWriter, r *http.Request) {
	// log.Println("setupWS")
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		// TODO: Correct error handling.
		return
	}

	// If we already have a status, send it.
	if status_str != nil{
		err := c.WriteMessage(websocket.TextMessage, status_str)
		if err != nil {
			log.Println("write:", err)
			return
		}
	}

	// Loop over channel 'current', when we get a new value, send it.
	for {
		status_str = <-current 
		err := c.WriteMessage(websocket.TextMessage, status_str)
		if err != nil {
			log.Println("write:", err)
			return
		}
	}
}

func static(w http.ResponseWriter, r *http.Request) {
	homeTemplate.Execute(w, "ws://"+*websocketEndPoint+"/status")
}

func monCurrent(current chan <- []byte) {
	for {
		log.Println("Calling MPC")
		cmd := exec.Command("mpc", "current", "--wait")
		log.Println("MPC Command created")
		tmp, err := cmd.Output()
		log.Println("Ran Command")
		if err != nil {
			log.Println("mpc:", err)
			panic(err)
		}
		log.Printf("%s", tmp)
		current <- tmp
		log.Println("End of monCurrent Loop")
	}
	log.Println("monCurrent exit")
}

func main() {
	flag.Parse()
	log.SetFlags(0)
	
	current = make(chan []byte, 1)
	go monCurrent(current)

	http.HandleFunc("/status", status)
	http.HandleFunc("/", static)
	log.Fatal(http.ListenAndServe(*addr, nil))

}

var homeTemplate = template.Must(template.New("").Parse(`
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <meta name="generator" content="pandoc" />
  <title></title>
  <style type="text/css">code{white-space: pre;}</style>
<script>  
window.addEventListener("load", function(evt) {
    var output = document.getElementById("output");
    var ws;
    var print = function(message) {
        var d = document.createElement("div");
        d.innerHTML = message;
        output.insertBefore(d, output.childNodes[0]);
    };

    if (ws) {
        return false;
    }

    ws = new WebSocket("{{.}}");

    ws.onmessage = function(evt) {
        print(evt.data);
        console.debug(evt.data);
    }

    ws.onerror = function(evt) {
        console.debug("ERROR: " + evt.data);
    }
    return false;
});
</script>

</head>
<body>
<h1 id="muse-music-service">Muse Music Service</h1>
<p>Listen to our MP3 stream 128bit (44100:16:1)</p>
<ul>
<li><a href="https://muse.port22.co.uk/listen.mp3">Central</a></li>
</ul>
<h1 id="admin">Admin</h1>
<ul>
<li><a href="https://muse.port22.co.uk/manage/home">Manage</a> (auth needed)</li>
</ul>
<h1 id="play-history">Play History</h1>
<div id="output"></div>
</body>
</html>
`))